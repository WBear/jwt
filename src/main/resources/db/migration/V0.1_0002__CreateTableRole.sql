CREATE TABLE public.role
(
    role_id     int8        NOT NULL,
    "name"      varchar(32) NOT NULL,
    CONSTRAINT role_pk PRIMARY KEY (role_id),
    CONSTRAINT role_un UNIQUE ("name")
);
