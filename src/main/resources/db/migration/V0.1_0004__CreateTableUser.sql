CREATE TABLE public.user
(
    user_id     int8        NOT NULL,
    username    varchar(64) NOT NULL,
    password    varchar(64) NOT NULL,
    first_name  varchar(64) NOT NULL,
    last_name   varchar(64) NOT NULL,
    active      bool        NOT NULL,
    CONSTRAINT user_pk PRIMARY KEY (user_id),
    CONSTRAINT user_un UNIQUE (username)
);