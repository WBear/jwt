do $$
declare
    v_user_id integer;
    v_admin_role_id integer;
begin
    v_user_id := NEXTVAL('hibernate_sequence');
    v_admin_role_id := (SELECT role_id FROM "role" WHERE "name" = 'ROLE_ADMIN');

    INSERT INTO public.user (user_id, username, password, first_name, last_name, active)
    VALUES (v_user_id, 'srckos@gmail.com', '$2a$10$UwT6Bdr1jwct5LeNKJgQ4.uFmXZHVCLErWdSZnEJxV5jX25OFWCKS', 'Srdjan', 'Sutinoski', true);

    INSERT INTO public.user_role
    (user_role_id, user_id, role_id)
    VALUES (NEXTVAL('hibernate_sequence'), v_user_id, v_admin_role_id);

end $$;