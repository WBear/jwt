CREATE TABLE user_role
(
    user_role_id        int8        NOT NULL DEFAULT nextval('hibernate_sequence'),
    user_id             int8        NOT NULL,
    role_id             int8        NOT NULL,
    CONSTRAINT user_role_pk PRIMARY KEY (user_role_id),
    CONSTRAINT user_role_un UNIQUE (user_id, role_id),
    CONSTRAINT user_role_fk2 FOREIGN KEY (user_id) REFERENCES public.user (user_id),
    CONSTRAINT user_role_fk1 FOREIGN KEY (role_id) REFERENCES role (role_id)
);