package rs.sutinoski.jwt.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
@Getter
public class ApplicationProperties {

    private final Security security = new Security();

    @Getter
    public static class Security {

        private final Authentication authentication = new Authentication();

        @Getter
        public static class Authentication {

            private final Jwt jwt = new Jwt();

            @Getter
            @Setter
            public static class Jwt {

                private String base64Secret;

                private long accessTokenValidityInSeconds;

                private long refreshTokenValidityInSeconds;

            }
        }
    }
}
