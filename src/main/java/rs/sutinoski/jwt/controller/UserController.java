package rs.sutinoski.jwt.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import rs.sutinoski.jwt.model.User;
import rs.sutinoski.jwt.service.UserService;
import rs.sutinoski.jwt.service.dto.UserRegistrationDTO;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/list")
    public ResponseEntity<List<User>> getUsers(Pageable pageable) {
        return ResponseEntity.ok().body(userService.getUsers(pageable).getContent());
    }

    @PostMapping("/save")
    public ResponseEntity<User> save(@RequestBody UserRegistrationDTO userDto) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/user/save").toUriString());
        return ResponseEntity.created(uri).body(userService.save(userDto));
    }


    @PutMapping("/update")
    public ResponseEntity<User> update(@RequestBody UserRegistrationDTO userDto) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/user/save").toUriString());
        return ResponseEntity.created(uri).body(userService.update(userDto));
    }

    @PostMapping("/add-role")
    public ResponseEntity<Void> addRole(@RequestBody RoleUser roleUser) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/user/add-role").toUriString());
        userService.assignRole(roleUser.username, roleUser.roleName);
        return ResponseEntity.created(uri).build();
    }

    @Data
    static class RoleUser {
        private String username;
        private String roleName;
    }

}
