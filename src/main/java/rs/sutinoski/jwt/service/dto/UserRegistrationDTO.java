package rs.sutinoski.jwt.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistrationDTO {

    private Long userId;

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private Set<String> roles = new HashSet<>();
}
