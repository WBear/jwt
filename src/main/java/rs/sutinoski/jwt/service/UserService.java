package rs.sutinoski.jwt.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.sutinoski.jwt.model.Role;
import rs.sutinoski.jwt.model.User;
import rs.sutinoski.jwt.service.dto.UserRegistrationDTO;

public interface UserService {

    User getByUsername(String username);

    User getById(Long userId);

    User save(UserRegistrationDTO user);

    User update(UserRegistrationDTO user);

    Role saveRole(Role role);

    void assignRole(String username, String roleName);

    Role getRole(String roleName);

    Page<User> getUsers(Pageable pageable);
}
