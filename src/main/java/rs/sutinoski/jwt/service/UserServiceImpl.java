package rs.sutinoski.jwt.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rs.sutinoski.jwt.error.ApplicationException;
import rs.sutinoski.jwt.model.Role;
import rs.sutinoski.jwt.model.User;
import rs.sutinoski.jwt.repository.RoleRepository;
import rs.sutinoski.jwt.repository.UserRepository;
import rs.sutinoski.jwt.service.dto.UserRegistrationDTO;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public User getByUsername(String username) {
        log.debug("Fetching user with username: {}...", username);
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            log.error("User with username {} don`t exist!", username);
            return null;
        }
        return user.get();
    }

    @Override
    public User getById(Long userId) {
        log.debug("Fetching user with id: {}...", userId);
        return userRepository.findById(userId).orElseThrow(
                () -> new ApplicationException("Unknown user")
        );
    }

    @Override
    @Transactional
    public User save(UserRegistrationDTO userDto) {
        log.debug("Saving new user: {} to db...", userDto.getUsername());
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setActive(true);

        Set<Role> roles = roleRepository.findByNameIn(userDto.getRoles().stream().toList());
        user.setRoles(roles);
        return userRepository.save(user);
    }

    @Override
    public User update(UserRegistrationDTO userDto) {
        log.debug("Updating user with id: {}...", userDto.getUserId());
        User user = userRepository.findById(userDto.getUserId()).orElseThrow(
                () -> new ApplicationException("Unknown user")
        );

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setActive(user.isActive());

        Set<Role> roles = roleRepository.findByNameIn(userDto.getRoles().stream().toList());
        user.setRoles(roles);
        return userRepository.save(user);
    }

    @Override
    public Role saveRole(Role role) {
        log.debug("Saving role: {} to db...", role.getName());
        return roleRepository.save(role);
    }

    @Override
    public void assignRole(String username, String roleName) {
        log.debug("Assigning role: {} to user: {}...", roleName, username);
        User user = userRepository.findByUsername(username).orElseThrow(() -> new RuntimeException("User don`t exist"));
        Role role = roleRepository.findByName(roleName).orElseThrow(() -> new RuntimeException("Role don`t exist"));
        user.getRoles().add(role);
    }

    @Override
    public Role getRole(String roleName) {
        log.debug("Fetching role: {}...", roleName);
        Optional<Role> role = roleRepository.findByName(roleName);
        if (role.isEmpty()) {
            log.error("Role with name {} don`t exist!", roleName);
            return null;
        }
        return role.get();
    }

    @Override
    public Page<User> getUsers(Pageable pageable) {
        log.debug("Getting users from db...");
        return userRepository.findAll(pageable);
    }

}
