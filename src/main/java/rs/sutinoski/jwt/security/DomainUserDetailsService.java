package rs.sutinoski.jwt.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import rs.sutinoski.jwt.error.UserNotActivatedException;
import rs.sutinoski.jwt.model.User;
import rs.sutinoski.jwt.repository.UserRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Optional;

@Slf4j
@Component("userDetailsService")
@RequiredArgsConstructor
public class DomainUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {
        log.debug("Authenticating {}", login);

        String lowercaseLogin = login.toLowerCase(Locale.ENGLISH);
        Optional<User> userDB = userRepository.findByUsername(lowercaseLogin);

        return userDB.map(user -> {
            if (!user.isActive()) {
                throw new UserNotActivatedException("User " + lowercaseLogin + " is inactive!");
            }

            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));

            SecurityUser securityUser = new SecurityUser(lowercaseLogin, user.getPassword(), authorities, user.getUserId());
            securityUser.setUserId(user.getUserId());
            return securityUser;
        }).orElseThrow(
                () -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the " + "database"));
    }
}
