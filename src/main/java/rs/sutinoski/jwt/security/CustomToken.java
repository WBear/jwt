package rs.sutinoski.jwt.security;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@EqualsAndHashCode(callSuper = true)
public class CustomToken extends UsernamePasswordAuthenticationToken {

    @Getter
    private final Long userId;

    public CustomToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities,
                       Long userId) {
        super(principal, credentials, authorities);
        this.userId = userId;
    }

}
