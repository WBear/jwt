package rs.sutinoski.jwt.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import rs.sutinoski.jwt.config.ApplicationProperties;
import rs.sutinoski.jwt.error.ApplicationException;
import rs.sutinoski.jwt.model.Role;
import rs.sutinoski.jwt.model.User;
import rs.sutinoski.jwt.service.UserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.stream;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthService {

    private final ApplicationProperties applicationProperties;

    private final UserService userService;

    public void accessToken(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getServletPath().equals("/api/authorize") || request.getServletPath().equals("/token/refresh")) {
            filterChain.doFilter(request, response);
        } else {
            String authorizationHeader = request.getHeader(AUTHORIZATION);
            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
                try {
                    String token = authorizationHeader.substring(7);
                    Algorithm algorithm = Algorithm.HMAC512(applicationProperties.getSecurity().getAuthentication().getJwt().getBase64Secret().getBytes());

                    JWTVerifier verifier = JWT.require(algorithm).build();
                    DecodedJWT decodedJWT = verifier.verify(token);

                    String tokenType = decodedJWT.getClaim("type").asString();
                    if (!tokenType.equals("ACCESS_TOKEN")) {
                        response.sendError(999, "Invalid token type");
                        throw new IllegalArgumentException("Wrong token type");
                    }

                    String[] roles = decodedJWT.getClaim("roles").asArray(String.class);
                    Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
                    stream(roles).forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));

                    SecurityUser user = new SecurityUser(decodedJWT.getSubject(), "", authorities, decodedJWT.getClaim("user").asLong());

                    Authentication authenticationToken = new UsernamePasswordAuthenticationToken(user, token, authorities);
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);

                    filterChain.doFilter(request, response);
                } catch (Exception ex) {
                    log.error("Error logging in: {}", ex.getMessage());
                    response.setHeader("ERROR", ex.getMessage());
                    response.setStatus(999);

                    Map<String, String> error = new HashMap<>();
                    error.put("error_message", ex.getMessage());
                    response.setContentType(APPLICATION_JSON_VALUE);
                    new ObjectMapper().writeValue(response.getOutputStream(), error);
                }
            } else {
                filterChain.doFilter(request, response);
            }
        }
    }

    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("Refreshing token...");
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                String token = authorizationHeader.substring(7);
                Algorithm algorithm = Algorithm.HMAC512(applicationProperties.getSecurity().getAuthentication().getJwt().getBase64Secret().getBytes());

                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(token);

                String tokenType = decodedJWT.getClaim("type").asString();
                if (!tokenType.equals("REFRESH_TOKEN")) {
                    response.sendError(999, "Invalid token type");
                    throw new IllegalArgumentException("Wrong token type");
                }

                String username = decodedJWT.getSubject();
                User user = userService.getByUsername(username);

                String accessToken = JWT.create()
                        .withSubject(user.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis() + applicationProperties.getSecurity().getAuthentication().getJwt().getAccessTokenValidityInSeconds() * 1000))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles", user.getRoles().stream().map(Role::getName).toList())
                        .withClaim("type", "ACCESS_TOKEN")
                        .sign(algorithm);
                String refreshToken = JWT.create()
                        .withSubject(user.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis() + applicationProperties.getSecurity().getAuthentication().getJwt().getRefreshTokenValidityInSeconds() * 1000))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("type", "REFRESH_TOKEN")
                        .sign(algorithm);

                Map<String, String> tokens = new HashMap<>();
                tokens.put("access_token", accessToken);
                tokens.put("refresh_token", refreshToken);
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            } catch (Exception ex) {
                log.error("Error refreshing token: {}", ex.getMessage());
                response.setHeader("ERROR", ex.getMessage());
                response.setStatus(999);

                Map<String, String> error = new HashMap<>();
                error.put("error_message", ex.getMessage());
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            throw new ApplicationException("Refresh token is missing");
        }
    }
}
