package rs.sutinoski.jwt.error;

import lombok.Getter;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ApplicationException.class, Exception.class})
    public InternalError handleException(final HttpServletResponse httpServletResponse, final Exception exception) {
        final InternalError internalError;
        if (exception instanceof ApplicationException applicationException) {
            internalError = new InternalError(applicationException.getErrorCode().getCode(), applicationException.getAppCode(), applicationException.getMessage());
            httpServletResponse.setStatus(applicationException.getErrorCode().getHttpStatus());
        } else {
            internalError = new InternalError(ErrorCode.RUNTIME_ERROR.getCode(), null, exception.getMessage());
            httpServletResponse.setStatus(ErrorCode.RUNTIME_ERROR.getHttpStatus());
        }
        return internalError;
    }

    @Getter
    static class InternalError {
        private final LocalDateTime timestamp;
        private final String errorCode;
        private final String appCode;
        private final String errorMessage;

        public InternalError(String errorCode, String appCode, String errorMessage) {
            this.timestamp = LocalDateTime.now();
            this.errorCode = errorCode;
            this.appCode = appCode;
            this.errorMessage = errorMessage;
        }
    }
}
