package rs.sutinoski.jwt.error;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class ApplicationException extends RuntimeException {
    private final ErrorCode errorCode;
    private final String appCode;

    public ApplicationException(String message, ErrorCode errorCode, String appCode) {
        super(message);
        log.error(message);
        this.errorCode = errorCode;
        this.appCode = appCode;
    }

    public ApplicationException(String message, ErrorCode errorCode) {
        super(message);
        log.error(message);
        this.errorCode = errorCode;
        this.appCode = null;
    }

    public ApplicationException(String message) {
        super(message);
        log.error(message);
        this.errorCode = ErrorCode.BAD_REQUEST;
        this.appCode = null;
    }
}
